#include <stdbool.h>
#include <stdio.h>

#include "structs/bmp.h"
#include "util/util.h"


void usage() {
    print_warning("Usage: ./print_header BMP_SOURCE BMP_DEST");
    print_warning("BMP_DEST should be typed with extension .bmp !");
}

int main(int argc, char **argv) {
    if (argc != 3 && argc != 2) usage();
    if (argc == 2) print_warning("The new pic will be created with default name");
    if (argc < 2) err("Not enough arguments \n");
    if (argc > 3) err("Too many arguments \n");

    struct image img = {0};
    if (!read_data_from_file(argv[1], &img)) {
        err("Failed during work!");
    }

    print_warning("Started rotating image");
    struct image rotated_image = image_rotate(img);
    print_info("Image rotated SUCCESSFULLY");

    const char *default_name = "i_hope_that_rotated_pic_with_cats.bmp";
    bool write_result;
    if (argc == 2) {
        write_result = write_data_to_file(default_name, &rotated_image);
    } else {
        write_result = write_data_to_file(argv[2], &rotated_image);
    }

    if (!write_result) {
        err("Failed during work");
    }

    image_free(&img);
    image_free(&rotated_image);

    return 0;
}
